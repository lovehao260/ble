<?php

Route::group([
    'prefix' => 'api/v1',
    'namespace' => 'Botble\Api\Http\Controllers',
    'middleware' => ['api'],
], function () {

    Route::post('login', 'ApiController@login');

    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('logout', 'ApiController@logout');
    });

});